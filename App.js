import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';

import Home from './components/views/home';
import Add from './components/views/add';

const App = StackNavigator({
  Home: { screen: Home },
  Add: { screen: Add },
});

export default App;