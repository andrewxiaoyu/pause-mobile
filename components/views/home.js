import React from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableHighlight, FlatList, BackHandler } from 'react-native';
import Swiper from 'react-native-swiper';
import Expo, { SQLite } from 'expo';

import Gallery from '../gallery';

const width =  Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;


const db = SQLite.openDatabase('db.db');

class Home extends React.Component {
    static navigationOptions = {
        header: null
    };
    constructor(props){
        super(props);
        this.state = {
            overlay: 0
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    }
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
        db.transaction(tx => {
            tx.executeSql(
                'create table if not exists pause (id integer primary key not null, name text, position int, status int);'
            );
        },
        e => console.log(e));
    }
    handleBackButton(){
        console.log("TEST");
        if(this.state.overlay == 0){
            BackHandler.exitApp();
        }
        else{
            this.closeItem();
            return true;
        }
    }
    addPause(type){
        const { navigate } = this.props.navigation;
        navigate('Add', { type: type });
    }
    openItem(type){
        this.setState({overlay: 1});
    }
    closeItem(type){
        this.setState({overlay: 0});
    }
    render() {
        const { params } = this.props.navigation.state;
        this.page = params ? parseInt(params.type)+1 : 1;

        let overlay;
        if(this.state.overlay == 1){
            overlay = <View style={styles.overlayContainer}>
                <View style={styles.overlay}>
                    <Text style={styles.name}>Title of Book</Text>
                    <Text style={styles.position}>40</Text>
                    <FlatList
                        columnWrapperStyle={styles.list}
                        numColumns={4}
                        data={[
                            {key: '+1'},
                            {key: '+5',},
                            {key: '+10'},
                            {key: '+50'},
                        ]}
                        renderItem={({item}) => 
                        <TouchableHighlight style={styles.overlayButton}>
                            <Text style={styles.buttonText}>{item.key}</Text>
                        </TouchableHighlight>
                        }
                    />
                    <FlatList
                        columnWrapperStyle={styles.bottomList}
                        numColumns={3}
                        data={[
                            {key: 'Save'},
                            {key: 'Delete'},
                            {key: 'Finish',},
                        ]}
                        renderItem={({item}) => 
                        <TouchableHighlight style={styles.bottomButton}>
                            <Text style={styles.bottomText}>{item.key}</Text>
                        </TouchableHighlight>
                        }
                    />
                </View>
            </View>
        }
        return (
            <View style={styles.container}>
                <Swiper style={styles.wrapper} index={this.page} showsPagination={false}>
                    <Gallery type={1} addPause={this.addPause.bind(this)} openItem={this.openItem.bind(this)}/>
                    <Gallery type={0} addPause={this.addPause.bind(this)} openItem={this.openItem.bind(this)}/>
                    <Gallery type={-1} addPause={this.addPause.bind(this)} openItem={this.openItem.bind(this)}/>
                </Swiper>
                {overlay}
            </View>
      );
    }
  }

export default Home;

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    wrapper:{
        flex:1
    },
    overlayContainer:{
        position: "absolute",
        // display:"none",
        top: 0,
        bottom: 0,
        height: height,
        width: width,
        flexDirection: "column",
        justifyContent:"center",
        backgroundColor: "#42465180",
        alignItems: "center"
    },
    overlay:{
        padding: 20,
        borderRadius: 5,
        width: width*.9,
        backgroundColor: "#AF3B6E",
        flexDirection: "column",
    },
    name:{
        color: "#BCE7FD",
        fontSize: 30,
        fontWeight:"bold",
    },
    position:{
        color: "#BCE7FD",
        fontSize: 50,
        marginVertical: 10,
    },
    list:{
        paddingVertical: 10,
        justifyContent: 'space-between',
    },
    overlayButton:{
        height: 60,
        width: 60,
        borderRadius:10,
        backgroundColor: "#424651",
        flexDirection: "column",
        justifyContent:"center",
        elevation: 10,
    },
    buttonText:{
        marginTop: -4,
        textAlign: "center",
        fontSize: 25,
        color: "#21FA90",
        fontWeight:"bold"
    },
    bottomList:{
        marginTop: 10,
        paddingVertical: 10,
        justifyContent: 'space-between',
    },
    bottomButton:{
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius:5,
        backgroundColor: "#424651",
        flexDirection: "column",
        justifyContent:"center",
        elevation: 10,
    },
    bottomText:{
        textAlign: "center",
        fontSize: 20,
        color: "#21FA90",
    },
});