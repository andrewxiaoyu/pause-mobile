import React from 'react';
import { StyleSheet, Text, View , Dimensions, TouchableHighlight, TextInput, Keyboard, TouchableWithoutFeedback} from 'react-native';
import Expo, { LinearGradient, SQLite } from 'expo';

const width =  Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;


const db = SQLite.openDatabase('db.db');

class Add extends React.Component {
    static navigationOptions = {
        header: null
    };
    constructor(props){
        super(props);
        this.state = { 
            title: '',
            position: "0",
        };
    }
    addPause(){
        let name = this.state.title;
        let pos = parseInt(this.state.position);
        let type = parseInt(this.props.navigation.state.params.type);

        if(name !== '' && pos !== ''){
            db.transaction(tx => {
                tx.executeSql('insert into pause (name, position, status) values (?, ?, ?)', 
                [name, pos, type]);
            },
            e => console.log(e),
            () => console.log("YAY"));

            const { navigate } = this.props.navigation;
            navigate('Home', { type: type });
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        let type = parseInt(this.props.navigation.state.params.type);
        let pos;
        if(type === 0){
            pos = <View>
                    <Text style={styles.title}>Position</Text>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                        <View>
                            <TextInput
                                underlineColorAndroid="transparent"
                                keyboardType='numeric'
                                style={styles.input}
                                onChangeText={(position) => this.setState({position:position.replace(/[^0-9]/g, '')})}
                                value={this.state.position}
                            />
                        </View>
                    </TouchableWithoutFeedback>
                </View>;
        }
        return (
            <View style={styles.wrapper}>
                <Text style={styles.title}>Title</Text>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                    <View>
                        <TextInput
                            underlineColorAndroid="transparent"
                            style={styles.input}
                            onChangeText={(title) => this.setState({title})}
                            value={this.state.title}
                            autoFocus={true}
                        />
                    </View>
                </TouchableWithoutFeedback>
                {pos}
                
                <View style={styles.buttonContainer}>
                <TouchableHighlight style={styles.button} onPress={this.addPause.bind(this)}>
                    <Text style={styles.plus}>+</Text>
                </TouchableHighlight>
                </View>
                <LinearGradient colors={['#C492B100', '#424651FF']} style={styles.overlayContainer} pointerEvents="none">
                </LinearGradient>
            </View>
      );
    }
  }

export default Add;

const styles = StyleSheet.create({
    wrapper:{
        flex:1,
        backgroundColor:"#AF3B6E",
        paddingTop: 50,
        paddingHorizontal: 30,
    },
    overlayContainer:{
        position: "absolute",
        bottom: 0,
        paddingVertical: 30,
        paddingHorizontal: 30,
        height: height/2,
        width: width,
        flexDirection: "column-reverse",
        alignItems: 'center',
    },
    title:{
        marginVertical: 10,
        color: "#BCE7FD",
        fontSize: 40,
        fontWeight:"bold",
    },
    input:{
        marginBottom: 20,
        backgroundColor: "#42465180",
        color: "#BCE7FD",
        fontSize: 20,
        padding: 10,
        paddingVertical: 15,
        borderRadius: 5,
    },
    buttonContainer:{
        alignItems:"center",
        marginVertical: 30
    },
    button:{
        height: 60,
        width: 60,
        borderRadius:30,
        backgroundColor: "#424651",
        flexDirection: "column",
        justifyContent:"center",
        elevation: 10,
    },
    plus:{
        marginTop: -4,
        textAlign:"center",
        fontSize:30,
        color: "#21FA90",
        fontWeight:"bold"
    }
});