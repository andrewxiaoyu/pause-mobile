import React from 'react';
import { StyleSheet, Text, View , Dimensions, FlatList, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo';

const width =  Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;

class Item extends React.Component {
    constructor(props){
        super(props);
    }
    openItem(){
        this.props.openItem();
    }
    render() {
        if(this.props.id === "headerSpace"){
            return(
                <View style={styles.headerSpace}></View>
            );
        }
        if(this.props.id === "footerSpace"){
            return(
                <View style={styles.footerSpace}></View>
            );
        }
        else{
            let pos;
            if(this.props.type === 0){
                pos = <Text style={[styles.text, styles.pause]}>{this.props.position}</Text>;
            }
            return (
                <TouchableOpacity style={styles.wrapper} onPress={this.openItem.bind(this)}>
                    <View>
                        <Text style={[styles.text, styles.name]}>{this.props.name}</Text>                            
                        {pos}
                    </View>
                </TouchableOpacity>
            );
        }
    }
  }

export default Item;

const styles = StyleSheet.create({
    headerSpace:{
        height:50,
        width: 10,
    },
    footerSpace:{
        height:200,
        width: 10,
    },
    wrapper:{
        height: width/2.5,
        width: width/2.5,
        backgroundColor: "#AF3B6E",
        borderRadius:5,
        margin: 10,
        padding: 20,
        justifyContent:"center",

        shadowOpacity: 0.5,
        shadowRadius: 5,
        shadowColor: '#424651',
        shadowOffset: { height: 0, width: 0 },
        elevation: 10,
        zIndex: 2
    },
    text:{
        textAlign:"center",
        color:"#BCE7FD"
    },
    name:{
        fontSize:20
    },
    pause:{
        fontSize:40
    }
});