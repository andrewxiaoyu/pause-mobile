import React from 'react';
import { StyleSheet, Text, View , Dimensions, FlatList, TouchableHighlight} from 'react-native';
import Expo, { LinearGradient, SQLite } from 'expo';

import Item from './listitems/item';

const width =  Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;

const nameMap = {
    "1": "Future",
    "0": "Present",
    "-1": "Past"
}

const db = SQLite.openDatabase('db.db');

class Gallery extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            items: []
        }
    }
    componentDidMount() {
        this.update();
    }
    update(){
        db.transaction(tx => {
            tx.executeSql(
                'select * from pause where status = ?;', [ this.props.type ],
                (_, { rows: { _array } }) => {
                    _array.unshift({
                        key: 'headerSpace',
                        name: '-1',
                        position: '-1',
                        status:'-1'
                    },
                    {
                        key: 'headerSpace',
                        name: '-1',
                        position: '-1',
                        status:'-1'
                    });
                    _array.push({
                        key: 'footerSpace',
                        name: '-1',
                        position: '-1',
                        status:'-1'
                    });
                    _array.push({
                        key: 'footerSpace',
                        name: '-1',
                        position: '-1',
                        status:'-1'
                    });
                    this.setState({ items: _array });
                }
            );
        },
        e => console.log(e));
    }
    addPause(){
        this.props.addPause(this.props.type);
    }
    openItem(){
        this.props.openItem(this.props.type);
    }
    render() {
        return (
            <View style={styles.wrapper}>
                <FlatList
                columnWrapperStyle={styles.list}
                numColumns={2}
                data={this.state.items}
                renderItem={({item}) => <Item id={item.key} name={item.name} position={item.position} type={item.status} openItem={this.openItem.bind(this)} />}
                keyExtractor={(item, index) => index}
                />
                
                <LinearGradient colors={['#C492B100', '#424651FF']} style={styles.overlayContainer} pointerEvents="none">
                </LinearGradient>
                <View style={styles.overlay}>
                    <Text style={styles.title}>{nameMap[this.props.type]}</Text>
                    <TouchableHighlight style={styles.button} onPress={this.addPause.bind(this)}>
                        <Text style={styles.plus}>+</Text>
                    </TouchableHighlight>
                </View>
            </View>
      );
    }
  }

export default Gallery;

const styles = StyleSheet.create({
    wrapper:{
        flex:1,
        backgroundColor:"#C492B1",
    },
    list:{
        justifyContent: 'flex-start',
        paddingHorizontal: 20,
        zIndex: 2
    },
    overlayContainer:{
        position: "absolute",
        bottom: 0,
        height: height/2,
        width: width,
        flexDirection: "column-reverse",
        zIndex: 0
    },
    overlay:{
        position:"absolute",
        bottom:0,
        width: width,
        paddingVertical: 30,
        paddingHorizontal: 30,
        flexDirection: "row",
        justifyContent:"space-between"
    },
    title:{
        color: "#21FA90",
        fontSize: 50,
        fontWeight:"bold"
    },
    button:{
        height: 60,
        width: 60,
        borderRadius:30,
        backgroundColor: "#424651",
        flexDirection: "column",
        justifyContent:"center",
        elevation: 10,
    },
    plus:{
        marginTop: -4,
        textAlign:"center",
        fontSize:30,
        color: "#21FA90",
        fontWeight:"bold"
    }
});